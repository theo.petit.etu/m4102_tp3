package fr.ulille.iut.pizzaland;

import java.util.logging.Logger;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.dao.PizzaDao;

public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
	private PizzaDao dao;
	
	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}
	
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createPizzaTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropAssociationTable();
		dao.dropPizzaTable();
	}
	
	@Test
	public void testGetIngredientsByPizzaId() {
		
	}
}
