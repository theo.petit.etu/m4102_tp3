package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (id INTEGER PRIMARY KEY, pizzaId INTEGER NOT NULL FOREIGN KEY REFERENCES Pizzas(id), ingredientId INTEGER NOT NULL FOREIGN KEY REFERENCES ingredients(id), CONSTRAINT FK_Pizza FOREIGN KEY ())")
	void createAssociationTable();
	
	@SqlUpdate("DROP TABLE PizzaIngredientsAssociation")
	void dropAssociationTable();
	
	@SqlUpdate("DROP TABLE Pizzas")
	void dropPizzaTable();
	
	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);
	
	@SqlQuery("SELECT i.id, i.name FROM PizzaIngredientsAssociation AS pIA INNER JOIN Ingredients AS i ON pIA.ingredientId = i.id WHERE pIA.pizzaId = :id")
	@RegisterBeanMapper(Ingredient.class)
	List<Ingredient> getIngredients(long id);

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createPizzaTable();
	}
}
