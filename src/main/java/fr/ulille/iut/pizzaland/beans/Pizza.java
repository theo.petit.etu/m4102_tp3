package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {
		super();
	}

	public Pizza(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public void addIngredient(Ingredient ingredient) {
		ingredients.add(ingredient);
	}
	
	public boolean removeIngredient(Ingredient ingredient) {
		return ingredients.remove(ingredient);
	}
	
	public static PizzaDto toDto(Ingredient i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setName(i.getName());

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());

		return pizza;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
