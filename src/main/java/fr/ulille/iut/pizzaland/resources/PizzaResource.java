package fr.ulille.iut.pizzaland.resources;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.GET;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientDto;

@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	private PizzaDao pizzas;
	
	@Context
	public UriInfo uriInfo;
	
	public PizzaResource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createPizzaTable();
	}
	
	@GET
	@Path("{id}/ingredients")
	public List<IngredientDto> getIngredients(@PathParam("id") long id) {
		LOGGER.info("IngredientResource:getAll");

		List<IngredientDto> l = pizzas.getIngredients(id).stream().map(Ingredient::toDto).collect(Collectors.toList());
		return l;
	}
}
